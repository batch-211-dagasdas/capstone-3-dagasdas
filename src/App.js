import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AppNavbar from "./components/AppNavbar.js";
import Home from "./pages/Home.js";
import Products from "./pages/Products.js";
import ProductView from "./components/ProductView.js";
import Register from "./pages/Register.js";
import Login from "./pages/Login.js";
import Logout from "./pages/Logout.js";
import AdminDash from "./pages/AdminDash.js";
import AdminUpdateProduct from "./components/AdminUpdateProduct.js";
import Error from "./pages/Error.js";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext.js";

import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`https://capstone-2-dagasdas.onrender.com/users/details`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(response => response.json())
    .then(data => {

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
    })
  }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/products/:productId/update" element={<AdminUpdateProduct/>}/>
            <Route path="/products/all" element={<AdminDash/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<Error/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
