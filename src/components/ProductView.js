import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import { useParams, useNavigate, Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext.js";
import Swal from "sweetalert2";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription]= useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const [isActive, setIsActive] = useState(false);

	function checkOut(e){

		e.preventDefault();

		fetch(`https://capstone-2-dagasdas.onrender.com/products/${productId}/createOrder`, {
			method:"POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity	
			})
		})
		.then(response => response.json())
		.then(data => {

			if(data === true){
				console.log(data);

				Swal.fire({
					title: `Product ordered!`,
					icon: "success",
					text: `"${name}" has been successfully ordered!`
				});

				navigate("/products");
			} else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"
				});
			}
		});
    }

	useEffect(() => {
		console.log(productId);
		fetch(`https://capstone-2-dagasdas.onrender.com/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [productId]);

	useEffect(() => {
	    if(quantity > 0){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }
	}, [quantity]);

	return (
		(user.isAdmin)
		?
		<Navigate to="/products/all" />
		: (user.id === null)
			?
			<Container className="mt-5">
				<Row>
					<Col lg={{span: 6, offset: 3}}>
					    <Card>
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PHP {price}</Card.Text>
							<Button as={Link} className="btn btn-danger" to="/login">
								Log in to Order
							</Button>
					      </Card.Body>
					    </Card>
				    </Col>
			    </Row>
		    </Container>
			:
			<Container className="mt-5">
				<Row>
					<Col lg={{span: 6, offset: 3}}>
					    <Card>
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PHP {price}</Card.Text>
							<Card.Subtitle>Quantity</Card.Subtitle>
							<Form onSubmit={(e) => checkOut(e)}>
					        	<Form.Group controlId="quantity" className="mb-3">
									<Form.Control 
									type="number" 
									value={quantity}
									placeholder="Enter Quantity" 
									onChange={e=>setQuantity(e.target.value)}
									required
									/>
								</Form.Group>
								<Card.Subtitle>Total Amount:</Card.Subtitle>
					        	<Card.Text>&#8369;{price * quantity}</Card.Text>
						        {
						        	(isActive)	
									?
									<Button variant="primary" type="submit" id="submitBtn" >
										Order
									</Button>
									:
									<Button variant="primary" type="submit" id="submitBtn" disabled>
										Order
									</Button>
						        }
					        </Form>
					      </Card.Body>
					    </Card>
				    </Col>
			    </Row>
		    </Container>
	)
}