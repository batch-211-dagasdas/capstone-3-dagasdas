// import coursesData from "../data/courses";
import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext.js";

export default function Products(){
	
	const [products, setProducts] = useState();

	const { user } = useContext(UserContext);

	useEffect(() => {
		fetch(`https://capstone-2-dagasdas.onrender.com/products/`)
		.then(response => response.json())
		.then(data => {

			const userProductArr = (data.map((userProduct) => {
				return (
					<ProductCard productProp={userProduct} key={userProduct._id}/>
				)
			}));
			setProducts(userProductArr);
		});
	}, [products]);

	return(
		(user.isAdmin !== true)
		?
		<>
			<h1>Products</h1>
			{products}
		</>
		:
		<Navigate to="/products/all"/>
	)
}