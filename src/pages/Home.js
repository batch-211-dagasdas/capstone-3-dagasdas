import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){

	const data = {
			title: "Shoes Available for you!!",
			content: "Footware that Suit youre style",
			destination: "/products",
			label: "Shop Now"
		}

	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
}