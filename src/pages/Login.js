import { Form, Button } from "react-bootstrap";
// Complete (3) Hooks of React
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext.js";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {

  const [email, setEmail] = useState ("");
  const [password, setPassword] = useState ("");
  const [isActive, setIsActive] = useState ("");

  // Allows us to consume the User Context/Data and its properties for validation
  const { user, setUser } = useContext(UserContext);


  function authenticate(e) {

      e.preventDefault();

      fetch(`https://capstone-2-dagasdas.onrender.com/users/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(response => response.json())
      .then(data => {
        if(typeof data.access !== "undefined"){
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to ComputerCorner! Enjoy shopping!"
          })
        } else{
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Please check your credentials."
          })
        }
      });

      const retrieveUserDetails = (token) => {

        fetch(`https://capstone-2-dagasdas.onrender.com/users/details`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }) 
        .then(response => response.json())
        .then(data => {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }

      setEmail("");
      setPassword("");
  }

  useEffect(()=>{
    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])



  return (
    (user.id !== null)
    ?
    <Navigate to="/products"/>
    :
    <Form onSubmit={(e)=>authenticate(e)}>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter your email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required
        />
      </Form.Group>
      
      { 
        isActive 
        ?
        <Button variant="success" type="submit" id="submitBtn">
          Login
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Login
        </Button>
      }

    </Form>
  );
}
